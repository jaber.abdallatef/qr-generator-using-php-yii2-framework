<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "qr".
 *
 * @property int $id
 * @property string $name
 * @property string $date
 * @property int $file
 *
 * @property Files $file0
 */
class Qrcodes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'qr';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'file'], 'required'],
            [['date'], 'safe'],
            [['file'], 'integer'],
            [['name', 'color'], 'string', 'max' => 15],
            [['file'], 'exist', 'skipOnError' => true, 'targetClass' => Files::className(), 'targetAttribute' => ['file' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'date' => 'Date',
            'file' => 'File',
        ];
    }

    /**
     * Gets query for [[File0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFile0()
    {
        return $this->hasOne(Files::className(), ['id' => 'file']);
    }
}
