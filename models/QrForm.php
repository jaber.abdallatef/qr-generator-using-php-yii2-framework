<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class QrForm extends Model
{
    public $number;
    public $color;
    public $prefix;
    public function rules()
    {
        return [
            [['number', 'color', 'prefix'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'number' => 'QR Number',
            'color'  =>  'QR Color',
            'prefix' => 'QR Prefix',
        ];
    }


    public function generateQr()
    {
        if ($this->validate()) {


            return true;
        }
        return false;
    }
}
