<?php
namespace app\components;

use Da\QrCode\QrCode;
use Yii;
use yii\base\Component;
use  app\models\Files;
use app\models\Qrcodes;
use kartik\mpdf\Pdf;
class QrHandler extends Component
{
    public function generateQrCode($model)
    {
        /// extract rgb from hexa 
        list($r, $g, $b) = sscanf($model->color, "#%02x%02x%02x");
        /// start building html content 
        $htmlImageComponent = "<div style='display: flex;
    flex-direction: column;
    align-items: center;'>" ; 
    /// store file on database 
     $file = new Files() ; 
     $file->name  = $model->prefix ; 
     $file->save();


        for ($i = 0 ; $i < $model->number; $i++) {

             /// make  qr codes unique ------- and save qr info on database 
             $qrModel = new Qrcodes() ; 
             $currentDateTime = date('Y-m-d H:i:s');
             $qrModel->name  = strtotime($currentDateTime).$model->prefix.$i.$file->id ;
             $qrModel->color = $model->color  ; 
             $qrModel->file  = $file->id  ; 
             $qrModel->save();  
             // -------------- //////  --------
            $htmlImageComponent .= "<img style='margin:1rem' src='codes/".  $qrModel->name. ".png' />" ;
            //-- array_push($qrArr , strtotime($currentDateTime) . $model->prefix.$i . '.png') ;
            //-- array things ---------
            $qrCode = (new QrCode($qrModel->name))
                ->useForegroundColor($r, $g, $b)
                ->useBackgroundColor(255, 255, 255)
                ->setSize(250)
                ->setMargin(5);
            $qrCode->writeFile(Yii::getAlias('@codesPath') . '/' . $qrModel->name. '.png');
        }
           $htmlImageComponent .=  "</div>";
     //----------------------------
        return $htmlImageComponent ; 
    }
}
