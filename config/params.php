<?php
Yii::setAlias('@webpath', realpath(dirname(__FILE__) . '/../web'));
Yii::setAlias('@codesPath', realpath(dirname(__FILE__) . '/../web/codes'));
Yii::setAlias('@pdfPath', realpath(dirname(__FILE__) . '/../web/pdf'));

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
];
