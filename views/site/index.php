<?php

use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'Qr Code Generator';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Qr Code Generator!</h1>

        <p class="lead"></p>

        <p>
        <?= Html::a('Generate Qr Codes', ['site/qr'], ['class' => 'btn btn-primary']) ?>
  
    </div>

    <div class="body-content">


    </div>
</div>
