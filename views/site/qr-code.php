<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use kartik\color\ColorInput;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Qr Generator';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-contact">
    <h1><?=Html::encode($this->title)?></h1> 
  
        <div class="row">
            <div class="col-lg-5">
                <?php $form = ActiveForm::begin(['id' => 'qr-form' ,
                  'options' => [ 'name' =>  'qr-form']])?>
                <?=$form->field($model, 'number')->textInput(['autofocus' => true])?>
                <?=$form->field($model, 'color')->widget(ColorInput::classname(), [
                'options' => ['placeholder' => 'Select color ...']]);?>
                    <?=$form->field($model, 'prefix')?>
                    <div class="form-group">
                        <?=Html::submitButton('Submit', ['class' => 'btn btn-primary' , 'onclick' => 'submitForm()', 'name' => 'qr-form-button'])?>
                         <input type="checkbox" id="moreThanPdf"> New Window On Submit
                    </div>

                <?php ActiveForm::end();?>
              
            </div>
        </div>


</div>
<script>
    function submitForm(){
        const cb = document.getElementById('moreThanPdf');
          if(cb.checked)
              window.open('index.php?r=site/qr')
    }
</script>